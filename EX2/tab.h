#ifndef __TAB_H__
#define __TAB_H__

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <time.h>

int** createTab(int width, int height);

void initTab(int** tab, int width, int height);

void deleteTab(int** tab, int height);

void afficheTab(int** tab, int width, int height);

int enregisterTab(int** tab, int width, int height, char *path);

int** lireFichier(char *path, int* width, int* height);

#endif