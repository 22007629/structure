#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#include "tab.h"

#define WIDTH 1000
#define HEIGHT 600

int main(int argc, const char **argv) {
    int** tab = createTab(WIDTH, HEIGHT);
    initTab(tab, WIDTH, HEIGHT);
    afficheTab(tab, WIDTH, HEIGHT);
    int stat = enregisterTab(tab, WIDTH, HEIGHT, "EX2/superFichier.bin");
    int width = 0, height = 0;
    int **tab2 = lireFichier("EX2/superFichier.bin", &width, &height);
    printf("\n");
    afficheTab(tab2, width, height);
    deleteTab(tab, HEIGHT);
    deleteTab(tab2, height);
    return 0;
}