#include "tab.h"

int** createTab(int width, int height) {
    int** tab = malloc(sizeof(int *) * height);
    for (int i = 0; i < height; i++) {
        tab[i] = malloc(sizeof(int *) * width);
    }
    return tab;
}

void initTab(int** tab, int width, int height) {
    if (tab == NULL) {
        printf("Error, tab is NULL");
        return;
    }
    srand(time(NULL));
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            tab[i][j] = rand() % 21;
        }
    }
}

void deleteTab(int** tab, int height) {
    if (tab == NULL) {
        printf("Error, tab is NULL");
        return;
    }
    for (int i = 0; i < height; i++) {
        free(tab[i]);
    }
    free(tab);
}

void afficheTab(int** tab, int width, int height) {
    if (tab == NULL) {
        printf("Error, tab is NULL");
        return;
    }
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            printf("%d, ", tab[i][j]);
        }
        printf("\n");
    }
}

int enregisterTab(int** tab, int width, int height, char *path) {
    FILE *f = fopen(path, "w");
    if (f == NULL) {
        printf("Error, file not open\n");
        return -1;
    }
    fwrite(&width, sizeof(int), 1, f);
    fwrite(&height, sizeof(int), 1, f);
    for (int i = 0; i < height; i++) {
        fwrite(tab[i], sizeof(int), width, f);
    }
    fclose(f);
    return 0;
}

int** lireFichier(char *path, int* width, int* height) {
    FILE *f = fopen(path, "r");
    if (f == NULL) {
        printf("Error, file not open\n");
        return NULL;
    }
    fread(width, sizeof(int), 1, f);
    fread(height, sizeof(int), 1, f);
    int **tab = createTab(*width, *height);
    for (int i = 0; i < *height; i++) {
        fread(tab[i], sizeof(int), *width, f);
    }
    fclose(f);
    return tab;
}