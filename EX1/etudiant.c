#include "etudiant.h"

etudiant_t *createEtudiant(char *nom, char *prenom, int year, unsigned int number) {
    etudiant_t *e = malloc(sizeof(etudiant_t));
    e->nom = malloc(sizeof(char) * (strlen(nom) + 1));
    strcpy(e->nom, nom);
    e->prenom = malloc(sizeof(char) * (strlen(prenom) + 1));
    strcpy(e->prenom, prenom);
    e->year = year;
    e->number = number;
    return e;
}

void deleteEtudiant(etudiant_t *e) {
    free(e->nom);
    free(e->prenom);
    free(e);
}

void printEtudiant(etudiant_t *e) {
    printf("%s, %s, %d, %u\n", e->nom, e->prenom, e->year, e->number);
}