#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "etudiant.h"

int main(int argc, char **argv) {
    FILE *f = fopen("file.csv", "r");
    if (f == NULL) {
        printf("Error opening\n");
        exit(0);
    }
    printf("Open\n");
    char mot[10];
    char letter = fgetc(f);
    char *liste[4];
    char counter = 0;
    char counterBis = 0;
    char counterBisBis = 0;
    char fisrtline = 0;
    etudiant_t **listEtudiant;
    while (letter != EOF) {
        if (fisrtline == 0) {
            if (letter == '\n') {
                fisrtline = 1;
            }
        } else if (letter != ',') {
            mot[counter] = letter;
            counter ++;
        } else if (letter != '\n') {
            liste[counterBis] = mot;
            for (int i = 0; i < 10; i++) {
                mot[i] = '\0';
            }
            counter = 0;
            counterBis ++;
            fgetc(f);
        } else {
            counter = 0;
            counterBis = 0;
            listEtudiant[counterBisBis] = createEtudiant(liste[0], liste[1], strtol(liste[2], NULL, 10), strtol(liste[3], NULL, 10));
            counterBisBis ++;
        }
        letter = fgetc(f);
    }
    fclose(f);
    printf("File reading\n");
    for (int i = 0; i < counterBisBis; i++) {
        printEtudiant(listEtudiant[i]);
    }
    for (int i = 0; i < counterBisBis; i++) {
        deleteEtudiant(listEtudiant[i]);
    }
    printf("\n");
    return 0;
}