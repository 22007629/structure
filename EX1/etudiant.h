#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#ifndef __ETUDIANT_H__
#define __ETUDIANT_H__

typedef struct etudiant_s {
    char *nom;
    char *prenom;
    int year;
    unsigned int number;
} etudiant_t;

etudiant_t *createEtudiant(char *nom, char *prenom, int year, unsigned int number);

void deleteEtudiant(etudiant_t *e);

void printEtudiant(etudiant_t *e);

#endif